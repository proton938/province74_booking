<?php
date_default_timezone_set('Asia/Yekaterinburg'); // часовой пояс по Екатеринбургу

if (isset($_REQUEST['sitenumber'])) {$sitenumber = $_REQUEST['sitenumber'];}


require_once '../../base/connection_base.php'; 

$payment_bufer = $db->query('SELECT * FROM hotel_accountpayments');    			// обращаемся к таблице истории платежей
$read_payment = $payment_bufer->fetchAll();


$count_read_payment = count($read_payment);

$count_plus = 0;

foreach ($read_payment as $payment_field)
	{
		if ($payment_field['SITENUMBER'] == $sitenumber && $payment_field['SUCCESS'] == 1)
			{
				$count_plus++;
				
				echo '<br>	Зявка : '.$payment_field['SITEPAY'].' обнулена<br>';
							
				$alphabet = '0123456789qwertyuiopasdfghjklzxcvbnm';				// генерируем идентификатор денежной операции
				$order_id = '';
				for ($j = 0; $j <= 20; $j++)
					{
						$order_id = $order_id.$alphabet[mt_rand(0, 35)];
					}
					
				$date_today = date("d.m.Y H:i");									

				$db->query('INSERT INTO hotel_accountpayments (	
																SUCCESS,
																FILE_NAME,
																SITEPAY,
																BANK_ORDER_ID,
																SITENUMBER,
																DATEDOC,
																IDPAYMENTTYPE,
																COMMENT,
																SUMMA,
																IDPROFITGROUP,
																ZEROIZE
																)	
																VALUES
																	  (	
																		"'.$payment_field['SUCCESS'].'",
																		"payment_'.($count_read_payment+$count_plus).'.xml",
																		"'.$order_id.'",
																		"'.$payment_field['BANK_ORDER_ID'].'",
																		"'.$sitenumber.'",
																		"'.$date_today.'",
																		"'.$payment_field['IDPAYMENTTYPE'].'",
																		"'.$payment_field['COMMENT'].'",
																		"'.(0-$payment_field['SUMMA']).'",
																		"11",
																		"1"
																			)
																			');
				//Создает XML-строку и XML-документ при помощи DOM 
				$dom = new DomDocument('1.0', 'UTF-8');
				
				//добавление корня - <data> 
				$data = $dom->appendChild($dom->createElement('data'));
				
				//добавление корня - <table> 
				$table = $data->appendChild($dom->createElement('table'));
				// добавление элемента текстового узла <table> в <table> 
				$table->appendChild($dom->createTextNode('HOTEL_ACCOUNTPAYMENTS'));
				
				//добавление корня - <row> 
				$row = $data->appendChild($dom->createElement('row'));
				
				//добавление элемента <SITEPAY> в <row> 
				$SITEPAY = $row->appendChild($dom->createElement('SITEPAY')); 
				// добавление элемента текстового узла <SITEPAY> в <SITEPAY> 
				$SITEPAY->appendChild($dom->createTextNode($order_id));
				
				//добавление элемента <IDSBERBANK> в <row> 
				$IDSBERBANK = $row->appendChild($dom->createElement('IDSBERBANK')); 
				// добавление элемента текстового узла <IDSBERBANK> в <IDSBERBANK> 
				$IDSBERBANK->appendChild($dom->createTextNode($payment_field['BANK_ORDER_ID']));
				
				//добавление элемента <SITENUMBER> в <row> 
				$SITENUMBER = $row->appendChild($dom->createElement('SITENUMBER')); 
				// добавление элемента текстового узла <SITENUMBER> в <SITENUMBER> 
				$SITENUMBER->appendChild($dom->createTextNode($sitenumber));
				
				//добавление элемента <DATEDOC> в <row> 
				$DATEDOC = $row->appendChild($dom->createElement('DATEDOC')); 
				// добавление элемента текстового узла <DATEDOC> в <DATEDOC> 
				$DATEDOC->appendChild($dom->createTextNode($date_today));
				
				//добавление элемента <IDPAYMENTTYPE> в <row> 
				$IDPAYMENTTYPE = $row->appendChild($dom->createElement('IDPAYMENTTYPE')); 
				// добавление элемента текстового узла <IDPAYMENTTYPE> в <IDPAYMENTTYPE> 
				$IDPAYMENTTYPE->appendChild($dom->createTextNode($payment_field['IDPAYMENTTYPE']));
				
				//добавление элемента <COMMENT> в <row> 
				$COMMENT = $row->appendChild($dom->createElement('COMMENT')); 
				// добавление элемента текстового узла <COMMENT> в <COMMENT> 
				$COMMENT->appendChild($dom->createTextNode($payment_field['COMMENT']));
				
				//добавление элемента <SUMMA> в <row> 
				$SUMMA = $row->appendChild($dom->createElement('SUMMA')); 
				// добавление элемента текстового узла <SUMMA> в <SUMMA> 
				$SUMMA->appendChild($dom->createTextNode((0-$payment_field['SUMMA'])));
				
				//добавление элемента <IDPROFITGROUP> в <row> 
				$IDPROFITGROUP = $row->appendChild($dom->createElement('IDPROFITGROUP')); 
				// добавление элемента текстового узла <IDPROFITGROUP> в <IDPROFITGROUP> 
				$IDPROFITGROUP->appendChild($dom->createTextNode(11));
				
				//генерация xml 
				$dom->formatOutput = true; // установка атрибута formatOutput
										   // domDocument в значение true 
				// save XML as string or file 
				$test1 = $dom->saveXML(); // передача строки в файл
				
				
				$dom->save('../../requests/import/payment_'.($count_read_payment+$count_plus).'.xml'); // сохранение файла 
			}
	}

	

	