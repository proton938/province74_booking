<meta charset = "utf-8">

<?php 
date_default_timezone_set('Asia/Yekaterinburg'); // часовой пояс по Екатеринбургу

require_once '../../base/connection_base.php';

$all_files = scandir('../../history');

echo '<br><br>'.count($all_files).'<br><br>';

for ($i=3; $i<count($all_files); $i++)
	{
		echo $all_files[$i].' ';
		
		$file = '../../history/'.$all_files[$i];														// открываем файл оперативной истории для чтения	
		$contents = file_get_contents($file); 														// считываем содержимое
		$contents = preg_split('/;/', $contents);													// разбиваем на массив по регулярному выражению ";"
		
		$date = new DateTime(date('Y-m-d H:i:s', filemtime($file)));		// определяем возраст сессии
		$year_ago = $date->diff(new DateTime)->format('%y');				// сколько лет
		$month_ago =  $date->diff(new DateTime)->format('%m');				// сколько месяцев
		$day_ago =  $date->diff(new DateTime)->format('%d');				// сколько дней
		$hour_ago = $date->diff(new DateTime)->format('%h');				// сколько часов
		$minute_ago = $date->diff(new DateTime)->format('%i');				// сколько минут
		
		
		if ($year_ago != 0 or $month_ago != 0 or $day_ago != 0 or $hour_ago != 0)    // если сессия не обновлялась больше часа
			{
				if (count($contents) <= 2)						// если в файле сессии не выбрано ни одной даты - можем просто удалять файл
					{
						unlink($file);
					}
				else											
					{
						/* если в файле сессии есть выбранные даты - 
						необходимо произвести очистку в базе 
						и только после этого можем удалить файл */
						
						for ($j = 1; $j < count($contents); $j++)
							{
								$booking = preg_split('/,/', $contents[$j]);
								
								$bufer_numbers = $db->query('SELECT * FROM numbers WHERE id = "'.$booking[0].'"');
								$read_numbers = $bufer_numbers->fetchAll(); 
								
								foreach ($read_numbers as $number_field)
									{
										$user = $number_field['user_'.$booking[2]];
									}
									
								if ($user == '')
									{		
										$db->query('UPDATE numbers SET number_'.$booking[2].' = ""  WHERE id = "'.$booking[0].'"');
									}
							}
							
						unlink($file);
					}
			}
		
		if ($minute_ago > 9)    // если сессия не обновлялась десять минут 
			{
				if (count($contents) > 2)						//  если в файле сессии есть выбранные даты - необходимо произвести очистку в базе 
					{
						for ($j = 1; $j < count($contents); $j++)
							{
								$booking = preg_split('/,/', $contents[$j]);
								
								$bufer_numbers = $db->query('SELECT * FROM numbers WHERE id = "'.$booking[0].'"');
								$read_numbers = $bufer_numbers->fetchAll(); 
								
								foreach ($read_numbers as $number_field)
									{
										$user = $number_field['user_'.$booking[2]];
									}
									
								if ($user == '')
									{		
										$db->query('UPDATE numbers SET number_'.$booking[2].' = ""  WHERE id = "'.$booking[0].'"');
									}
							}
						$date_today = date("H:i:s_d.m.Y");
						
						$f = fopen('../../history/'.$all_files[$i], 'w');
						fwrite($f, $date_today.";\n");									// Пишем текущую дату и время в файл оперативной истории
						fclose($f);
					}
			}
	}



?>