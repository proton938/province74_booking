<?php 

date_default_timezone_set('Asia/Yekaterinburg'); // часовой пояс по Екатеринбургу

$date_today = date("Y-m-d-H-i");

echo $date_today;

//Создает XML-строку и XML-документ при помощи DOM 
$dom = new DomDocument('1.0', 'UTF-8');

//добавление корня - <data> 
$data = $dom->appendChild($dom->createElement('data'));

//добавление корня - <table> 
$table = $data->appendChild($dom->createElement('table'));
// добавление элемента текстового узла <table> в <table> 
$table->appendChild($dom->createTextNode($date_today));

//генерация xml 
$dom->formatOutput = true; // установка атрибута formatOutput
						   // domDocument в значение true 
// save XML as string or file 
$test1 = $dom->saveXML(); // передача строки в файл

$dom->save('../requests/import/test_'.$date_today.'.xml'); // сохранение файла 

?>